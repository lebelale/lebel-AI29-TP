# AI29 - TP2 - 2023/2024

## 1 - Les bases

__1.1 Comment l'information est-elle représentée dans un système binaire classique ?__

L'information est représentée par des bits, qui sont des espaces qui peuvent prendre uniquement deux valeurs : 0 ou 1. Ces bits sont regroupés en octets, qui sont des groupes de 8 bits. 

__1.2 Les opérations logiques de base__

- NOT : inverse la valeur du bit
- AND : si les deux bits sont à 1, alors le résultat est 1, sinon 0
- OR : si au moins un des deux bits est à 1, alors le résultat est 1, sinon 0
- XOR : si les deux bits sont différents, alors le résultat est 1, sinon 0

__2 Donner les états possibles d'un Qubit et expliquer $\ket{\Psi} = \alpha \ket{0} + \beta \ket{1}$__

Un Qubit peut prendre deux états : $\ket{0}$ ou $\ket{1}$.
Lorsque le qubit n'est pas mesuré, son état n'est pas fixé, il est dans un état de superposition. C'est à dire qu'il est à la fois dans l'état $\ket{0}$ et dans l'état $\ket{1}$, avec des probabilités $\alpha$ et $\beta$ respectivement. Lorsque le qubit est mesuré, il prend un état définitif, soit $\ket{0}$, soit $\ket{1}$, avec des probabilités $\alpha^2$ et $\beta^2$ respectivement.

__3.3 Affichage du circuit__

![Circuit simple avec porte Hadamard](images/img1.png)

## 2 - Les portes

__1.1 Définition des portes quantiques__

- H (Hadamard) : Permet de passer le Qubit dans un état de superposition
- X (NOT) : Permet d'inverser l'état du Qubit
- CNOT : Permet d'inverser l'état d'un Qubit en utilisant l'état d'un Qubit "contrôleur". L'opération `NOT` est donc effectuée lorsque le Qubit contrôle est dans l'état $\ket{1}$
- SWAP : La porte SWAP permet d'échanger les états de deux Qubits. Ainsi, si l'état de base est $\ket{01}$ alors après la porte SWAP, l'état sera $\ket{10}$
- CSWAP : La porte CSWAP réalise l'opération d'échange lorsque le Qubit "contrôle" est à l'état $\ket{1}$
- CCNOT : La porte CCNOT permet d'appliquer l'opération NOT lorsque deux Qubits "contrôle" sont à l'état $\ket{1}$

__1.2 Matrices de passage__

- H : $\frac{1}{\sqrt{2}} \begin{pmatrix} 1 & 1 \\ 1 & -1 \end{pmatrix}$
- X : $\begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}$
- CNOT : $\begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \\ 0 & 0 & 1 & 0 \end{pmatrix}$
- SWAP : $\begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \end{pmatrix}$
- CSWAP : $\begin{pmatrix} 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\ 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \end{pmatrix}$ 
- CCNOT : $\begin{pmatrix} 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\ 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\ 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \end{pmatrix}$

__1.3 Code et affichage du circuit__

```python
from qiskit import QuantumCircuit

# Création d’un circuit quantique instance de QuantumCircuit
circ = QuantumCircuit(3)
# Ajouter une porte H pour l’entrée (qubit) 0 => superposition
circ.h(0)
# Porte NOT
circ.x(2)
# Porte CNOT
circ.cx(0, 1)
# Porte SWAP
circ.swap(1,2)
# Porte CSWAP
circ.cswap(2,0,1)
# Porte CCNOT
circ.ccx(0,1,2)


circ.draw('mpl')
```

![Circuit avec toutes les portes](images/img2.png)

__3 Ajout de paramètres__

```python
from qiskit import *
from qiskit.visualization import *

def create_quantum_circuit(nqubits, nbits, **gates):
    # Création d’un circuit quantique instance de QuantumCircuit
    circ = QuantumCircuit(nqubits, nbits)
    for key, value in gates.items():
        if 'cx' in key:
            circ.cx(value[0], value[1])
        if 'x' in key:
            circ.x(value)
        if 'h' in key:
            circ.h(value)
        if 'm' in key:
            circ.measure(value[0], value[1]) 
    return circ

circ_data = {
    'h' : 0,
    'cx' : [0,1],
    'm' : [1, 0],
    'm1' : [0,1],
}

circ = create_quantum_circuit(2, 2, **circ_data)


circ.draw('mpl')
```

### Simulations

__Circuit 1__ :

![Schéma du circuit 1](images/img3.png)

![Histogramme du circuit 1](images/img4.png)

![Sphère de Bloch](images/img5.png)

__Circuit 2__ :

![Schéma du circuit 2](images/img6.png)

![Histogramme du circuit 2](images/img7.png)

![Sphère de Bloch](images/img8.png)


### Circuits de SWAP et CSWAP

La matrice de l'opération SWAP est la suivante : 

$$\begin{split}\mathrm{SWAP} =
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 1 & 0 & 0\\
0 & 0 & 0 & 1
\end{pmatrix}\end{split}$$

Ce qui signifie que l'on peut la retrouver en utilisant d'autres portes dont la CNOT :

$$\begin{split}C_X(0,1) =
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & 1 & 0 & 0\\
0 & 0 & 0 & 1\\
0 & 0 & 1 & 0
\end{pmatrix}\end{split} \ et \ \begin{split}C_X(1,0) =
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & 0 & 0 & 1\\
0 & 0 & 1 & 0\\
0 & 1 & 0 & 0
\end{pmatrix}\end{split}$$

On a, par multiplication de matrices :

$$C_x(0,1) . C_x(1,0) . C_x(0,1) = SWAP(0,1)$$

Un circuit possible est donc :

```python
circ_data = {
    'cx' : [0,1],
    'cx1' : [1,0],
    'cx2' : [0,1],
}
```

Qui donne :

![Circuit swap à partir de trois porte](images/img9.png)

Similairement, la matrice de l'opération SWAP est la suivante :

$$\begin{split}C_{\mathrm{SWAP}} =
\begin{pmatrix}
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1
\end{pmatrix}\end{split}$$

Et la matrice de l'opération CCNOT est la suivante :

$$\begin{split}C_{CX}(0,1,2) =
\begin{pmatrix}
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0
\end{pmatrix}\end{split}$$

Et on a :

$$C_{CX}(0,1,2) . C_{CX}(0,2,1) . C_{CX}(0,1,20) = \mathrm{CSWAP}(0,1,2)$$

Cela donne le circuit suivant :

```python
circ_data = {
    'ccx' : [0,1,2],
    'ccx1' : [0,2,1],
    'ccx2' : [0,1,2],
}
```

![Circuit d'un CSWAP à 3 portes](images/img10.png)

Evidemment, il est possible d'utiliser les portes directement :

```python
circ_data = {
    'sw' : [0,1],
}
```

![Circuit d'un SWAP](images/img12.png)

```python
circ_data = {
    'cs' : [0,1,2],
}
```

![Circuit d'un CSWAP](images/img11.png)


## 3 - Etat de Bell

![Circuit simple avec porte Hadamard](images/img1.png)

Etat de Bell : Si on connait le premmier Qubit, on connais le deuxième

Cas 1 :
$$
\text{Entrées : } \ket{00} \\
H : \ket{q_0} = \frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1} \\
\frac{1}{\sqrt{2}}(\ket{0} + \ket{1}) \otimes \ket{0} = \frac{1}{\sqrt{2}}(\ket{00} + 0.\ket{01} + 0.\ket{10} + \ket{11}) \\
= \begin{pmatrix} \frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}} \end{pmatrix} \\
\text{CNOT : } \frac{1}{\sqrt{2}}(\ket{00} + \ket{11}) \\
$$

Cas 2 :

$$
\text{Entrées : } \ket{01} \\
H \ :\frac{1}{\sqrt{2}}(\ket{0} + \ket{1}) \otimes \ket{1} = \frac{1}{\sqrt{2}}(\ket{01} + \ket{11}) \\
\text{CNOT : } \frac{1}{\sqrt{2}}(\ket{01} + \ket{10}) \\
\\
$$

Cas 3 :

$$
\text{Entrées : } \ket{10} \\
H \ :\frac{1}{\sqrt{2}}(\ket{0} - \ket{1}) \otimes \ket{1} = \frac{1}{\sqrt{2}}(\ket{00} - \ket{10}) \\
\text{CNOT : } \frac{1}{\sqrt{2}}(\ket{00} - \ket{11}) \\
\\
$$

Cas 4 :

$$
\text{Entrées : } \ket{11} \\
H \ :\frac{1}{\sqrt{2}}(\ket{0} - \ket{1}) \otimes \ket{1} = \frac{1}{\sqrt{2}}(\ket{01} - \ket{11}) \\
\text{CNOT : } \frac{1}{\sqrt{2}}(\ket{01} - \ket{10}) \\
\\
$$


## Exercice 4

Code : 

```py
circ2 = {
    'h' : 1,
    'x' : 2,
    'cx2' : [1, 2],
    'cx1': [0,1],
    'h2' : 0,
}

from qiskit.visualization import *

circ3 = circ2.copy()

circ2 = create_quantum_circuit(3, 2, **circ2)

circ3['m'] = [1,0]
circ3['m1'] = [0,1]

circ3 = create_quantum_circuit(3, 2, **circ3)

circ3.x(2).c_if(0,1)
circ3.z(2).c_if(1,1)

circ3.draw('mpl') 
```

![Circuit de Téléportation](images/img13.png)

```py
from qiskit.quantum_info import DensityMatrix
backend = BasicAer.get_backend('statevector_simulator')
job = execute(circ3, backend).result()
plot_bloch_multivector(job.get_statevector(circ3), title="Téléportation quantique")
```

![Resultat de la téléportation](images/img14.png)

Le premier qubit change d'état, il n'est pas cloné