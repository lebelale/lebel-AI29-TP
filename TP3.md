# Algorithme de Deutsh-Josza

$$
f : x \rightarrow y=f(x)
$$

$$\{0,1\}^n \rightarrow \{0,1\}$$

$$ constante \rightarrow  \begin{cases}
f(x) = 0 \forall x \\
f(x) = 1 \forall x
\end{cases}$$

$$ équilibrée \rightarrow \begin{cases} 
2^{n-1} \rightarrow f(x) = 0 \\
2^{n-1} \rightarrow f(x) = 1
\end{cases}
$$

![Synoptique de l'algorithme](images/2.png)

Un oracle permet d'implémenter un circuit qui a le même comportement que la fonction qu'on cherche à tester. Comment trouver cet oracle pour trouver la fonction transformée ?

![Exemple d'Oracle équlibré](images/3.png)

__Table de vérité__

|x|y|
|-|-|
|000|0|
|001|1|
|010|1|
|011|0|
|etc|etc|

